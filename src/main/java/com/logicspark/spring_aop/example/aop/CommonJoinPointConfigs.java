package com.logicspark.spring_aop.example.aop;

import org.aspectj.lang.annotation.Pointcut;

public class CommonJoinPointConfigs {

    @Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void controllerExecution() {}
}
