package com.logicspark.spring_aop.example.aop;

import com.logicspark.spring_aop.example.service.LogService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;

@Aspect
@Component
public class LogAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogAspect.class);

    private final LogService logService;

    public LogAspect(LogService logService) {
        this.logService = logService;
    }

    @Around("com.logicspark.spring_aop.example.aop.CommonJoinPointConfigs.controllerExecution()")
    public Object logControllerExecution(ProceedingJoinPoint joinPoint) throws Throwable {
        String methodName = joinPoint.getSignature().getName() + "()";
        String className = joinPoint.getSignature().getDeclaringType().getName();
        String fullMethodName = className + "." + methodName;
        String args = Arrays.toString(joinPoint.getArgs());
        RequestMapping requestMappingAnnotation = ((MethodSignature) joinPoint.getSignature()).getMethod()
                .getAnnotation(RequestMapping.class);
        String tmp = requestMappingAnnotation.value().length > 0 ? requestMappingAnnotation.value()[0] : "";
        LOGGER.info("{} - Endpoint called = {}: {} - [Payload:{}]", fullMethodName,
                Arrays.toString(requestMappingAnnotation.method()), tmp, args);
        long startTime = System.nanoTime();
        Object result = joinPoint.proceed();
        long endTime = System.nanoTime();

        long executionTime = (endTime - startTime) / 1000000;

        logService.save(fullMethodName, executionTime);

        LOGGER.info("{} - ResponseJson - {}", fullMethodName, result != null ? result.toString() : "");
        LOGGER.info("{} - End call", fullMethodName);

        return result;
    }
}
