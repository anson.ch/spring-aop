package com.logicspark.spring_aop.example.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class ExecutionLog {

    @Id
    @GeneratedValue
    private Long id;
    private LocalDateTime createDatetime;
    private String description;
    private Long executionTime;

    public ExecutionLog() {}

    public ExecutionLog(String description, long executionTime) {
        this.description = description;
        this.executionTime = executionTime;
        this.createDatetime = LocalDateTime.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(LocalDateTime createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Long executionTime) {
        this.executionTime = executionTime;
    }

    @Override
    public String toString() {
        return "ExecutionLog{" +
                "id=" + id +
                ", createDatetime=" + createDatetime +
                ", description='" + description + '\'' +
                ", executionTime=" + executionTime +
                '}';
    }
}
