package com.logicspark.spring_aop.example.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Audited
public class Profile {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String occupation;
    private String description;

    public Profile() {}

    public Profile(Long id, String name, String occupation, String description) {
        this.id = id;
        this.name = name;
        this.occupation = occupation;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", occupation='" + occupation + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
