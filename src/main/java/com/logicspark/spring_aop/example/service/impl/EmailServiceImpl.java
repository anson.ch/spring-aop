package com.logicspark.spring_aop.example.service.impl;

import com.logicspark.spring_aop.example.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailServiceImpl implements EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);

    private final JavaMailSender emailSender;
    private final SpringTemplateEngine thymeleafTemplateEngine;

    public EmailServiceImpl(JavaMailSender emailSender, SpringTemplateEngine thymeleafTemplateEngine) {
        this.emailSender = emailSender;
        this.thymeleafTemplateEngine = thymeleafTemplateEngine;
    }

    @Override
    public void send(String to, String subject, String htmlBody, String pathToAttachment) {
        assert !ObjectUtils.isEmpty(to);

        File attachment = null;
        if (!ObjectUtils.isEmpty(pathToAttachment)) {
            attachment = new File(pathToAttachment);
        }
        sendHtmlMessage(to, subject, htmlBody, attachment);
    }

    @Override
    public void sendWithThymeleaf(String to, String subject, Map<String, Object> templateModel, String pathToAttachment) {
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(templateModel);

        String htmlBody = thymeleafTemplateEngine.process("default-template.html", thymeleafContext);

        File attachment = null;
        if (!ObjectUtils.isEmpty(pathToAttachment)) {
            attachment = new File(pathToAttachment);
        }

        sendHtmlMessage(to, subject, htmlBody, attachment);
    }

    @Override
    public void test() {
        sendHtmlMessage("test@test.com", "this is a test", "Testing", null);
    }

    @Override
    public void testWithAttachment() throws IOException {
        File attachment = new ClassPathResource("attachment/testEmail.txt").getFile();
        sendHtmlMessage("test@test.com", "this is a test", "Testing", attachment);
    }

    @Override
    public void testWithThymeleaf() {
        Context thymeleafContext = new Context();

        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("recipientName", "Receiver Bob");
        templateModel.put("text", "This is just a test");
        templateModel.put("senderName", "Tester Charlie");
        thymeleafContext.setVariables(templateModel);

        String htmlBody = thymeleafTemplateEngine.process("default-template.html", thymeleafContext);

        sendHtmlMessage("test@test.com", "this is a test", htmlBody, null);
    }

    private void sendHtmlMessage(String to, String subject, String htmlBody, File attachment) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(htmlBody, true);

            if (attachment != null) {
                helper.addAttachment("test.txt", attachment);
            }

            emailSender.send(message);
        } catch (MessagingException e) {
            LOGGER.error("Error sending email", e);
            throw new RuntimeException("Error sending email");
        }
    }

}
