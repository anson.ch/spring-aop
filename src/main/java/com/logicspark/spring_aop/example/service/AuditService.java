package com.logicspark.spring_aop.example.service;

import java.util.List;

public interface AuditService {

    List<Object> findAllRevById(String entity, Long id) throws ClassNotFoundException;
    List<Object> findAllEntitiesByRev(String entity, Integer rev) throws ClassNotFoundException;
}
