package com.logicspark.spring_aop.example.service.impl;

import com.logicspark.spring_aop.example.config.ExampleConfig;
import com.logicspark.spring_aop.example.service.AuditService;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuditServiceImpl implements AuditService {

    private final ExampleConfig config;
    private final AuditReader auditReader;

    public AuditServiceImpl(ExampleConfig config, AuditReader auditReader) {
        this.config = config;
        this.auditReader = auditReader;
    }

    @Override
    public List<Object> findAllRevById(String entity, Long id) throws ClassNotFoundException {
        Class<?> clazz = config.getEntityClass(entity);
        AuditQuery query = auditReader.createQuery().forRevisionsOfEntity(clazz, true, true);
        query.add(AuditEntity.id().eq(id));

        return query.getResultList();
    }

    @Override
    public List<Object> findAllEntitiesByRev(String entity, Integer rev) throws ClassNotFoundException {
        Class<?> clazz = config.getEntityClass(entity);
        AuditQuery query = auditReader.createQuery().forEntitiesAtRevision(clazz, rev);

        return query.getResultList();
    }
}
