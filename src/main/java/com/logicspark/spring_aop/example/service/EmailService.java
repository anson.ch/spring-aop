package com.logicspark.spring_aop.example.service;

import java.io.IOException;
import java.util.Map;

public interface EmailService {

    void send(String to, String subject, String htmlBody, String pathToAttachment);
    void sendWithThymeleaf(String to, String subject, Map<String, Object> templateModel, String pathToAttachment);

    void test();
    void testWithAttachment() throws IOException;
    void testWithThymeleaf();
}
