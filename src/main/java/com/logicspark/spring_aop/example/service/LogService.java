package com.logicspark.spring_aop.example.service;

public interface LogService {

    void save(String description, long executionTime);
}
