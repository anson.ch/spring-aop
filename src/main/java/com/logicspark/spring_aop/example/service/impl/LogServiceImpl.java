package com.logicspark.spring_aop.example.service.impl;

import com.logicspark.spring_aop.example.domain.ExecutionLog;
import com.logicspark.spring_aop.example.repository.ExecutionLogRepository;
import com.logicspark.spring_aop.example.service.LogService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class LogServiceImpl implements LogService {

    private final ExecutionLogRepository executionLogRepository;

    public LogServiceImpl(ExecutionLogRepository executionLogRepository) {
        this.executionLogRepository = executionLogRepository;
    }

    @Override
    @Async
    public void save(String description, long executionTime) {
        ExecutionLog log = new ExecutionLog(description, executionTime);
        executionLogRepository.save(log);
    }
}
