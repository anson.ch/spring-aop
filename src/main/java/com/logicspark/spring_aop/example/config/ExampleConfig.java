package com.logicspark.spring_aop.example.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "example")
public class ExampleConfig {

    private Map<String, String> entityClassMapping;

    public String getEntityClassName(String entity) {
        return entityClassMapping.getOrDefault(entity, "");
    }

    public Class<?> getEntityClass(String entity) throws ClassNotFoundException {
        String className = getEntityClassName(entity);
        return Class.forName(className);
    }

    public Map<String, String> getEntityClassMapping() {
        return entityClassMapping;
    }

    public void setEntityClassMapping(Map<String, String> entityClassMapping) {
        this.entityClassMapping = entityClassMapping;
    }
}
