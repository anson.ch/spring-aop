package com.logicspark.spring_aop.example.repository;

import com.logicspark.spring_aop.example.domain.ExecutionLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExecutionLogRepository extends CrudRepository<ExecutionLog, Long> {
}
