package com.logicspark.spring_aop.example.repository;

import com.logicspark.spring_aop.example.domain.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends CrudRepository<Profile, Long> {
}
