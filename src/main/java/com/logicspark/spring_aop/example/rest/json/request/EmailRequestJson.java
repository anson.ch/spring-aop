package com.logicspark.spring_aop.example.rest.json.request;

import java.util.Map;

public class EmailRequestJson {

    private String to;
    private String subject;
    private String body;
    private String localPathToAttachment;
    private Map<String, Object> templateModel;

    public Map<String, Object> getTemplateModel() {
        return templateModel;
    }

    public void setTemplateModel(Map<String, Object> templateModel) {
        this.templateModel = templateModel;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getLocalPathToAttachment() {
        return localPathToAttachment;
    }

    public void setLocalPathToAttachment(String localPathToAttachment) {
        this.localPathToAttachment = localPathToAttachment;
    }
}
