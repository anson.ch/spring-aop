package com.logicspark.spring_aop.example.rest.controller;

import com.logicspark.spring_aop.example.service.AuditService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("audit")
public class AuditController {

    private final AuditService auditService;

    public AuditController(AuditService auditService) {
        this.auditService = auditService;
    }

    @RequestMapping(value = "/{entity}/{id}", method = RequestMethod.GET)
    public List<Object> findAllRevById(@PathVariable(value = "entity") String entity,
                                       @PathVariable(value = "id") String id) throws ClassNotFoundException {
        return auditService.findAllRevById(entity, Long.parseLong(id));
    }

    @RequestMapping(value = "/{entity}/rev/{rev}", method = RequestMethod.GET)
    public List<Object> findAllEntitiesByRev(@PathVariable(value = "entity") String entity,
                                             @PathVariable(value = "rev") String rev) throws ClassNotFoundException {
        return auditService.findAllEntitiesByRev(entity, Integer.parseInt(rev));
    }
}
