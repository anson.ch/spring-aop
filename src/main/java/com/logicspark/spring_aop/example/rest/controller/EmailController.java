package com.logicspark.spring_aop.example.rest.controller;

import com.logicspark.spring_aop.example.rest.json.request.EmailRequestJson;
import com.logicspark.spring_aop.example.service.EmailService;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("email")
public class EmailController {

    private final EmailService emailService;

    public EmailController(EmailService emailService) {
        this.emailService = emailService;
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public String send(@RequestBody EmailRequestJson requestJson, @RequestParam(required = false) String thymeleaf) {
        if (!ObjectUtils.isEmpty(thymeleaf) && "true".equals(thymeleaf)) {
            emailService.sendWithThymeleaf(requestJson.getTo(), requestJson.getSubject(), requestJson.getTemplateModel(),
                    requestJson.getLocalPathToAttachment());
        } else {
            emailService.send(requestJson.getTo(), requestJson.getSubject(), requestJson.getBody(),
                    requestJson.getLocalPathToAttachment());
        }
        return "Email sending success: " + requestJson.getTo();
    }

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public void test() {
        emailService.test();
    }

    @RequestMapping(value = "/testWithAttachment", method = RequestMethod.POST)
    public void testWithAttachment() throws IOException {
        emailService.testWithAttachment();
    }

    @RequestMapping(value = "/testWithThymeleaf", method = RequestMethod.POST)
    public void testWithThymeleaf() {
        emailService.testWithThymeleaf();
    }
}
