DROP TABLE IF EXISTS Profile;

CREATE TABLE Profile (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(250) NOT NULL,
    occupation VARCHAR(250),
    description VARCHAR(500)
);

INSERT INTO Profile (id, name, occupation, description) VALUES
    ('111', 'Tom', 'Student', 'Student at NUS'),
    ('222', 'John', 'Doctor', 'Save lives'),
    ('333', 'Adam', 'Businessman', 'Billionaire');


DROP TABLE IF EXISTS ExecutionLog;

CREATE TABLE ExecutionLog (
    id INT AUTO_INCREMENT PRIMARY KEY,
    create_datetime datetime not null,
    description VARCHAR(1000),
    execution_time BIGINT
);