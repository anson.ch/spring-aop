# Spring showcase
This project is meant to be a showcase for the following:
1. Spring AOP
2. Hibernate Envers
3. Spring Data Rest
4. Spring mail (with and without Thymeleaf)
5. Swagger (still work-in-progress)



## Endpoints
### Spring Data Rest
<table>
    <tr>
        <th>Request Method</th>
        <th>Endpoint</th>
        <th>Payload</th>
        <th>Remarks</th>
    </tr>
    <tr>
        <td>GET</td>
        <td>/</td>
        <td></td>
        <td>Display available api endpoints exposed by Spring Data Rest</td>
    </tr>
</table>



### Spring mail

<table>
    <tr>
        <th>Request Method</th>
        <th>Endpoint</th>
        <th>Payload</th>
        <th>Remarks</th>
    </tr>
    <tr>
        <td>POST</td>
        <td>/email/test</td>
        <td></td>
        <td>Test sending simple email</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>/email/testWithAttachment</td>
        <td></td>
        <td>Test sending with email attachment</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>/email/testWithThymeleaf</td>
        <td></td>
        <td>Test sending email with Thymeleaf template</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>/email/send</td>
        <td>
<pre>{
    "to": "test@test.com",
    "subject": "Testing send email",
    "body": "This is a test send email",
    "localPathToAttachment": "src/main/resources/attachment/testEmail.txt"
}</pre>
        </td>
        <td>Send email</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>/email/send?thymeleaf=true</td>
        <td>
<pre>{
    "to": "test@test.com",
    "subject": "Testing send email",
    "templateModel": {
        "recipientName": "Receiver Bobby",
        "text": "This is a simple test for sending email using Thymeleaf",
        "senderName": "Tester Charlie"
        },
    "localPathToAttachment": "src/main/resources/attachment/testEmail.txt"
}</pre>
        </td>
        <td>Send email with thymeleaf template</td>
    </tr>
</table>
